<?php

declare(strict_types=1);

namespace TaskReminder\Application\Command;

use TaskReminder\Domain\TodoId;
use TaskReminder\Domain\TodoDescription;


final class MarkTodoAsDone
{
    public TodoId $id;

    public function toArray()
    {
        return [
            'id' => $this->id->toString(),
        ];
    }
}
