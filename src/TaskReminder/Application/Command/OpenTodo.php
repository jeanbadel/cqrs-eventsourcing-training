<?php

declare(strict_types=1);

namespace TaskReminder\Application\Command;

use TaskReminder\Domain\TodoId;
use TaskReminder\Domain\TodoDescription;


final class OpenTodo
{
    public TodoId $id;

    public TodoDescription $description;

    public function toArray()
    {
        return [
            'id' => $this->id->toString(),
            'description' => $this->description->toString(),
        ];
    }
}
