<?php

declare(strict_types=1);

namespace TaskReminder\Application\Command;

use TaskReminder\Application\Command\OpenTodo;
use TaskReminder\Domain\Todo;
use TaskReminder\Domain\TodoRepository;

final class OpenTodoHandler
{
    private TodoRepository $repository;

    public function __construct(TodoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(OpenTodo $command): void
    {
        $todo = Todo::open($command->id, $command->description);

        $this->repository->add($todo);
    }
}
