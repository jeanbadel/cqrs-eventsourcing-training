<?php

declare(strict_types=1);

namespace TaskReminder\Application\Command;

use TaskReminder\Domain\TodoRepository;
use TaskReminder\Application\Command\MarkTodoAsDone;

final class MarkTodoAsDoneHandler
{
    private TodoRepository $repository;

    public function __construct(TodoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(MarkTodoAsDone $command): void
    {
        $todo = $this->repository->get($command->id);

        $todo->markAsDone();

        $this->repository->add($todo);
    }
}
