<?php

declare(strict_types=1);

namespace TaskReminder\Application\Query;

use TaskReminder\Application\ReadModel\TodoList;

final class GetAllTodosHandler
{
    public function __construct(TodoList $readModel)
    {
        $this->readModel = $readModel;
    }

    public function __invoke(): array
    {
        return $this->readModel->all();
    }
}
