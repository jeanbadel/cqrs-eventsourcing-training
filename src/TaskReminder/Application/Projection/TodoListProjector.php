<?php

declare(strict_types=1);

namespace TaskReminder\Application\Projection;

use TaskReminder\Application\ReadModel\TodoList;
use TaskReminder\Domain\TodoWasMarkedAsDone;
use TaskReminder\Domain\TodoWasOpened;

final class TodoListProjector
{
    private TodoList $readModel;

    public function __construct(TodoList $readModel)
    {
        $this->readModel = $readModel;
    }

    public function __invoke(object $event): void
    {
        switch (get_class($event)) {
            case TodoWasOpened::class:
                $this->readModel->add($event->id()->toString(), $event->description()->toString());
                break;

            case TodoWasMarkedAsDone::class:
                $this->readModel->markAsDone($event->id()->toString());
                break;
        }
    }
}
