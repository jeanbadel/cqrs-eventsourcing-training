<?php

declare(strict_types=1);

namespace TaskReminder\Application\ReadModel;

interface TodoList
{
    public function add(string $id, string $description): void;

    public function markAsDone(string $id): void;
}
