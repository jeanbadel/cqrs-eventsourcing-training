<?php

declare(strict_types=1);

namespace TaskReminder\Domain;

use TaskReminder\Domain\TodoId;
use TaskReminder\Domain\TodoDescription;
use DateTimeImmutable;
use DateTimeInterface;

final class TodoWasOpened
{
    private TodoId $id;

    private TodoDescription $description;

    private DateTimeImmutable $happenedAt;

    private function __construct(TodoId $id, TodoDescription $description)
    {
        $this->id = $id;
        $this->description = $description;
        $this->happenedAt = new DateTimeImmutable();
    }

    public static function happens(TodoId $id, TodoDescription $description): self
    {
        return new self($id, $description);
    }

    public function id(): TodoId
    {
        return $this->id;
    }

    public function description(): TodoDescription
    {
        return $this->description;
    }

    public function toArray(): array
    {
        return [
            'aggregate_id' => $this->id->toString(),
            'type' => static::class,
            'happened_at' => $this->happenedAt->format(DateTimeInterface::ATOM),
            'payload' => [
                'description' => $this->description->toString(),
            ]
        ];
    }

    public static function fromArray(array $data): self
    {
        $self = new self(
            TodoId::fromString($data['aggregate_id']),
            TodoDescription::fromString($data['payload']['description'])
        );
        $self->happenedAt = DateTimeImmutable::createFromFormat(DateTimeInterface::ATOM, $data['happened_at']);

        return $self;
    }
}
