<?php

declare(strict_types=1);

namespace TaskReminder\Domain;

final class Todo
{
    private array $recordedEvents = [];

    private TodoId $id;

    private bool $completed;

    private function __construct(TodoId $id, TodoDescription $description)
    {
        $this->recordThat(TodoWasOpened::happens($id, $description));
    }

    public static function open(TodoId $id, TodoDescription $description): self
    {
        return new self($id, $description);
    }

    public function markAsDone(): void
    {
        if ($this->completed) {
            throw new TodoAlreadyMarkedAsDone($this->id);
        }

        $this->recordThat(TodoWasMarkedAsDone::happens($this->id));
    }

    public function id(): TodoId
    {
        return $this->id;
    }

    public function recordedEvents(): array
    {
        return $this->recordedEvents;
    }

    public function replay(array $history): void
    {
        foreach ($history as $event) {
            $this->apply($event);
        }
    }

    private function recordThat(object $event): void
    {
        $this->recordedEvents[] = $event;
        $this->apply($event);
    }

    private function apply(object $event): void
    {
        switch (get_class($event)) {
            case TodoWasOpened::class:
                $this->id = $event->id();
                $this->completed = false;
                break;

            case TodoWasMarkedAsDone::class:
                $this->completed = true;
                break;
        }
    }
}
