<?php

declare(strict_types=1);

namespace TaskReminder\Domain;

use TaskReminder\Domain\Todo;
use TaskReminder\Domain\TodoId;

interface TodoRepository
{
    public function get(TodoId $id): ?Todo;

    public function add(Todo $todo): void;
}
