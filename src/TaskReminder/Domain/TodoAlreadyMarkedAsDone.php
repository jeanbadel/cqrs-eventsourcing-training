<?php

declare(strict_types=1);

namespace TaskReminder\Domain;

final class TodoAlreadyMarkedAsDone extends \Exception
{
    public function __construct(TodoId $id)
    {
        parent::__construct(sprintf('Todo "%s" is already marked as done', $id->toString()));
    }
}
