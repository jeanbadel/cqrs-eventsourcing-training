<?php

declare(strict_types=1);

namespace TaskReminder\Domain;

use Assert\Assertion;

final class TodoDescription
{
    private string $value;

    private function __construct(string $value)
    {
        Assertion::notEmpty($value);

        $this->value = $value;
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    public function toString(): string
    {
        return $this->value;
    }
}
