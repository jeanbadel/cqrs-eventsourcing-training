<?php

declare(strict_types=1);

namespace TaskReminder\Domain;

use TaskReminder\Domain\TodoId;
use TaskReminder\Domain\TodoDescription;
use DateTimeImmutable;
use DateTimeInterface;

final class TodoWasMarkedAsDone
{
    private TodoId $id;

    private DateTimeImmutable $happenedAt;

    private function __construct(TodoId $id)
    {
        $this->id = $id;
        $this->happenedAt = new DateTimeImmutable();
    }

    public static function happens(TodoId $id): self
    {
        return new self($id);
    }

    public function id(): TodoId
    {
        return $this->id;
    }

    public function toArray(): array
    {
        return [
            'aggregate_id' => $this->id->toString(),
            'type' => static::class,
            'happened_at' => $this->happenedAt->format(DateTimeInterface::ATOM),
            'payload' => []
        ];
    }

    public static function fromArray(array $data): self
    {
        $self = new self(TodoId::fromString($data['aggregate_id']));
        $self->happenedAt = DateTimeImmutable::createFromFormat(DateTimeInterface::ATOM, $data['happened_at']);

        return $self;
    }
}
