<?php

declare(strict_types=1);

namespace TaskReminder\Infrastructure;

use TaskReminder\Domain\TodoRepository;
use TaskReminder\Domain\Todo;
use TaskReminder\Domain\TodoId;

final class InMemoryEventStoreTodoRepository implements TodoRepository
{
    private array $storage = [];

    private $eventBus;

    public function __construct(callable $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function add(Todo $todo): void
    {
        $key = $todo->id()->toString();
        if (!isset($this->storage[$key])) {
            $this->storage[$key] = [];
        }

        $recordedEvents = $todo->recordedEvents();
        // Append only storage !
        $this->storage[$key] = array_merge($this->storage[$key], array_map(fn(object $event): array => $event->toArray(), $recordedEvents));

        $eventBus = $this->eventBus;
        foreach ($recordedEvents as $event) {
            $eventBus($event);
        }
    }

    public function get(TodoId $id): ?Todo
    {
        if (!isset($this->storage[$id->toString()])) {
            return null;
        }

        $history = array_map(
            fn (array $event): object => $event['type']::fromArray($event),
            $this->storage[$id->toString()]
        );
        $todo = (new \ReflectionClass(Todo::class))->newInstanceWithoutConstructor();
        $todo->replay($history);

        return $todo;
    }
}
