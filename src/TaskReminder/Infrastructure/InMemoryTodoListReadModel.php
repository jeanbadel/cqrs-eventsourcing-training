<?php

declare(strict_types=1);

namespace TaskReminder\Infrastructure;

use TaskReminder\Application\ReadModel\TodoList;

final class InMemoryTodoListReadModel implements TodoList
{
    private array $storage = [];

    public function add(string $id, string $description): void
    {
        $this->storage[$id] = ['description' => $description, 'done' => false];
    }

    public function markAsDone(string $id): void
    {
        $this->storage[$id]['done'] = true;
    }

    public function all(): array
    {
        return $this->storage;
    }
}
