<?php

// In order to focus on things that remain to be done
// As an user
// I need to be able to mark a todo item as done

require __DIR__ . '/../vendor/autoload.php';
$container = require __DIR__ . '/container.php';


$commandBus = $container['command.bus'];
$queryBus = $container['query.bus'];

$id = \TaskReminder\Domain\TodoId::generate();
$command = new \TaskReminder\Application\Command\OpenTodo();
$command->id = $id;
$command->description = \TaskReminder\Domain\TodoDescription::fromString('Buy some bread');
$commandBus($command);

$command = new \TaskReminder\Application\Command\MarkTodoAsDone();
$command->id = $id;
$commandBus($command);

$command = new \TaskReminder\Application\Command\MarkTodoAsDone();
$command->id = $id;
try {
    $commandBus($command);
} catch (\TaskReminder\Domain\TodoAlreadyMarkedAsDone $exception) {
    echo $exception->getMessage() . "\n";
    die(0);
}

throw new \Exception('Exception "TodoAlreadyMarkedAsDone" was expected.');
