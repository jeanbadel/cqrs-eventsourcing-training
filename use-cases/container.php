<?php

$container = [];

//Read repository definitions
$container[\TaskReminder\Application\ReadModel\TodoList::class] = new \TaskReminder\Infrastructure\InMemoryTodoListReadModel();

// Event Bus definition
$container['event.handlers'] = [
    function() use ($container) {
        return new \TaskReminder\Application\Projection\TodoListProjector($container[\TaskReminder\Application\ReadModel\TodoList::class]);
    }
];

$container['event.bus'] = (function(array $handlers): callable {
    return function(object $event) use ($handlers): void {
        foreach ($handlers as $handler) {
            $handler()($event);
        }
    };
})($container['event.handlers']);

//Write repository definitions
$container[\TaskReminder\Domain\TodoRepository::class] = new \TaskReminder\Infrastructure\InMemoryEventStoreTodoRepository($container['event.bus']);

// Command Bus definition
$container['command.handlers'] = [
    \TaskReminder\Application\Command\OpenTodo::class => function() use ($container) {
        return new \TaskReminder\Application\Command\OpenTodoHandler($container[\TaskReminder\Domain\TodoRepository::class]);
    },
    \TaskReminder\Application\Command\MarkTodoAsDone::class => function() use ($container) {
        return new \TaskReminder\Application\Command\MarkTodoAsDoneHandler($container[\TaskReminder\Domain\TodoRepository::class]);
    },
];

$container['command.bus'] = (function(array $handlers): callable {
    return function(object $command) use ($handlers): void {
        if (!isset($handlers[get_class($command)])) {
            throw new \InvalidArgumentException(sprintf('No handler found for command "%s"', get_class($command)));
        }

        $handlers[get_class($command)]()($command);
    };
})($container['command.handlers']);

// Query Bus definition
$container['query.handlers'] = [
    \TaskReminder\Application\Query\GetAllTodos::class => function() use ($container) {
        return new \TaskReminder\Application\Query\GetAllTodosHandler($container[\TaskReminder\Application\ReadModel\TodoList::class]);
    },
];

$container['query.bus'] = (function(array $handlers): callable {
    return function(object $query) use ($handlers) {
        if (!isset($handlers[get_class($query)])) {
            throw new \InvalidArgumentException(sprintf('No handler found for query "%s"', get_class($query)));
        }

        return $handlers[get_class($query)]()($query);
    };
})($container['query.handlers']);

return $container;
