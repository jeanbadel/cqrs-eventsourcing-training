<?php

// In order to remember all the things that I have to do
// As an user
// I need to be able to open todos

require __DIR__ . '/../vendor/autoload.php';
$container = require __DIR__ . '/container.php';


$commandBus = $container['command.bus'];
$queryBus = $container['query.bus'];

$id = \TaskReminder\Domain\TodoId::generate();
$command = new \TaskReminder\Application\Command\OpenTodo();
$command->id = $id;
$command->description = \TaskReminder\Domain\TodoDescription::fromString('Buy some bread');
$commandBus($command);
