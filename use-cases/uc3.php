<?php

// In order to know what must be done
// As an user
// I need to be able to display a todo list

require __DIR__ . '/../vendor/autoload.php';
$container = require __DIR__ . '/container.php';


$commandBus = $container['command.bus'];
$queryBus = $container['query.bus'];

$id = \TaskReminder\Domain\TodoId::generate();
$command = new \TaskReminder\Application\Command\OpenTodo();
$command->id = $id;
$command->description = \TaskReminder\Domain\TodoDescription::fromString('Buy some bread');
$commandBus($command);

$command = new \TaskReminder\Application\Command\MarkTodoAsDone();
$command->id = $id;
$commandBus($command);

$id = \TaskReminder\Domain\TodoId::generate();
$command = new \TaskReminder\Application\Command\OpenTodo();
$command->id = $id;
$command->description = \TaskReminder\Domain\TodoDescription::fromString('Pick up the car from the garage');
$commandBus($command);


$query = new \TaskReminder\Application\Query\GetAllTodos();
$todos = $queryBus($query);

foreach ($todos as $todo) {
    printf('  - [%s] %s' . "\n", $todo['done'] ? 'x' : ' ', $todo['description']);
}
