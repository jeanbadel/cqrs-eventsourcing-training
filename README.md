# Introduction

This pet project aims to introduce the concepts of separating write and read models.

It is illustrated through basic use cases, that pictures the development of a Todo application.

Use cases, presented in the `use-cases` directory are supposed to be explored in alphabetical order.

The different implementations are purposely simples, and not supposed to be used in production!

A more thorough introduction of CQRS and Event Sourcing, by [Greg Young](https://twitter.com/gregyoung), is available [here](https://www.youtube.com/watch?v=JHGkaShoyNs).

# Installation

Run the following command to install dependencies:

```bash
$ docker run --rm -it -v "$PWD":/app -v "$HOME/.composer":/tmp --user "$(id -u):$(id -g)" composer install
```

# Run the use cases

Individual use case can be run through the following command:

```bash
$ docker run --rm -it -v "$PWD":/app --user "$(id -u):$(id -g)" -w /app prooph/php:7.4-cli-xdebug php use-cases/uc1.php
```
